# Prometheus SNMP Exporter

This project has been archived, as it has been incorporated into the main [cluster management](https://gitlab.com/memhamwan/cluster-management) repository in these two MRs ([!1](https://gitlab.com/memhamwan/cluster-management/-/merge_requests/2), [!2](https://gitlab.com/memhamwan/cluster-management/-/merge_requests/3)). Please refer to there for future actions.
